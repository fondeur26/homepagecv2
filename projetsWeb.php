<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="img/favicon.png">
  <link rel="stylesheet" href="css/mobile.css" media="screen and (max-width: 750px)">
  <link rel="stylesheet" href="css/index.css" media="screen and (min-width: 751px)">
  <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet">
  <title>HERVE FONDEUR</title>
</head>

<body class="fond03">

  <div id="page">
    <header>
      <div class="margeGauche">
        <p>Site pour la SPA en landing page</p>
        <a href="https://landingpagespa.vercel.app/">
        <img src="img/projetsWeb02.png"alt="landing page spa"/></a>
        <p>Site de drum startwars</p>
        <a href="https://drumstarwars.vercel.app/">
        <img src="img/projetsWeb01.png" alt="drum startwars"/></a>
        <a href="http://portefolio.herve-fondeur.labo-ve.fr/">
        <p>
        <img src="img/projetsWeb03.png" alt="portefolio"/></a>
        </p>
        
      </div>
    </header>

    <div class="margeDroite">
      <section>
        <h2><a href="presentation.php">Présentation</a></h2>
      </section>

      <section>
        <h2><a href="domainesDeCompetences.php">Domaines de Compétences</a></h2>
      </section>

      <section>
        <h2><a href="projetsWeb.php">Projets Web</a></h2>
      </section>

      <section>
        <h2><a href="formations.php">Formations</a></h2>
      </section>

      <section>
        <h2><a href="experiencesProfessionnelles.php">Expériences Professionnelles</a></h2>
      </section>

      <section>
        <h2><a href="contact.php">Contact</a></h2>
      </section>

      <section>
        <h2><a href="img/CVHFR.pdf">Mon CV</a></h2>
      </section>
    </div>
  </div>

</body>
</html>
