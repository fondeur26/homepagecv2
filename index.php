<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="img/favicon.png">
  <link rel="stylesheet" href="css/mobile.css" media="screen and (max-width: 750px)">
  <link rel="stylesheet" href="css/index.css" media="screen and (min-width: 751px)">
  <link href="https://fonts.googleapis.com/css?family=Permanent+Marker" rel="stylesheet">
  <title>HERVE FONDEUR</title>
</head>

<body class="pageAccueil">

<div id="sideNavigation" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="presentation.php">presentation</a>
  <a href="#">Features</a>
  <a href="#">Contact Us</a>
</div>
 
<nav class="topnav">
  <a href="#" onclick="openNav()">
    <svg width="30" height="30" id="icoOpen">
        <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>
        <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>
        <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>
    </svg>
  </a>
</nav>
 
<div id="main">

</div>
  <div>
    <h1><a href="presentation.php">hervé Fondeur</h1>
    
    <h1><a href="presentation.php">Développeur Web</h1>
    
    <p class="submit">
      			<button href="presentation.php">la suite</button>
    </p>
  </div>

  <video autoplay loop class="video">
  <source src="img/accueil.mp4" type="video/mp4">
  </video>
  <script src="js/script.js"></script>
</body>
</html>
