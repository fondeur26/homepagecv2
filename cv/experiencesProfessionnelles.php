<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="img/favicon.png">
  <link rel="stylesheet" href="css/mobile.css" media="screen and (max-width: 750px)">
  <link rel="stylesheet" href="css/index.css" media="screen and (min-width: 751px)">
  <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet">
  <title>HERVE FONDEUR</title>
</head>

<body class="fond05">

  <div id="page">
    <header>
      <div class="margeGauche">
        <section>
          <p>Depuis 2018 - Trésorier bénévole et Membre du CA</p>
          <p>Groupement Fnath Sud Est  -Valence 26000</p>
        </section>

        <section>
          <p>2006 à 2018</p>
          <p>Chef produit secteur et responsable développement</p>
          <p>EXPERT PONT TV -Pont-Evêque 38780</p>
        </section>

        <section>
          <p>2000 à 2017</p>
          <p>Sapeur-pompier volontaire-Evolution de Sapeur àSergent </p>
          <p>CIS Saint-Uze - 26240 et Ministère de l’intérieur Paris</p>
        </section>
      </div>
    </header>

    <div class="margeDroite">
      <section>
        <h2><a href="presentation.php">Présentation</a></h2>
      </section>

      <section>
        <h2><a href="domainesDeCompetences.php">Domaines de Compétences</a></h2>
      </section>

      <section>
        <h2><a href="projetsWeb.php">Projets Web</a></h2>
      </section>

      <section>
        <h2><a href="formations.php">Formations</a></h2>
      </section>

      <section>
        <h2><a href="experiencesProfessionnelles.php">Expériences Professionnelles</a></h2>
      </section>

      <section>
        <h2><a href="contact.php">Contact</a></h2>
      </section>

      <section>
        <h2><a href="img/CVHFR.pdf">Mon CV</a></h2>
      </section>
    </div>
  </div>

</body>
</html>
