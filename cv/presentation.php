<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="img/favicon.png">
  <link rel="stylesheet" href="css/mobile.css" media="screen and (max-width: 750px)">
  <link rel="stylesheet" href="css/index.css" media="screen and (min-width: 751px)">
  <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet">
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  <title>HERVE FONDEUR</title>
</head>

<body class="fond01">
<div id="content">

    <span class="slide">
      <a href="#" onclick="openSlideMenu()">
        <i class="fas fa-bars"></i>
      </a>
    </span>

    <div id="menu" class="nav">
      <a href="#" class="close" onclick="closeSlideMenu()">
        <i class="fas fa-times"></i>
      </a>
      <a href="#">Home</a>
      <a href="presentation.php">Présentation</a>
      <a href="domainesDeCompetences.php">Domaines de Compétences</a>
      <a href="projetsWeb.php">Projets Web</a>
      <a href="formations.php">Formations</a>
      <a href="experiencesProfessionnelles.php">Expériences Professionnelles</a>
      <a href="contact.php">Contact</a>
      <a href="img/CVHFR.pdf">Mon CV</a>
    </div>

    
    

  </div>
  <div id="page">
    <header>
      <div class="margeGauche">
        <h2>Développeur Web & Mobile</h2>
        <div>
        <img class="photoMobile" src="img/photoMoi.jpeg" alt="Photo de moi"/>
        </div>
        <div>
        <p>Passionné d'informatique, grâce à 20 ans
          d’exercices, j'aime découvrir de nouvelles
          technologies, les assimiler et les utiliser à
          travers le développement, le déploiement
          et l’aide à l’utilisation.<br>
          ‘’ Un CODE pour tous, tous pour un CODE’’</p>
          </div>
      </div>
    </header>

    <script src="js/script.js"></script>

</body>
</html>
